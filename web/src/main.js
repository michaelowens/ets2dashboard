import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		rows: 4,
		cols: 4,
	}
});

export default app;