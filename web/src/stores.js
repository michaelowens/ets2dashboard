import { writable } from 'svelte/store'

export const name = writable('world')
export const canvas = writable({
  items: [
    {
      type: 'Button',
      text: 'One',
      position: { x: 20, y: 20 },
      size: { w: 100, h: 100 },
    },
    {
      type: 'Button',
      text: 'Two',
      position: { x: 140, y: 20 },
      size: { w: 80, h: 80 },
    },
    {
      type: 'Button',
      text: 'Three',
      position: { x: 260, y: 20 },
      size: { w: 100, h: 100 },
    },
  ],
})
